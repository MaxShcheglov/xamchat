﻿using Moq;
using MvvmCross.Core.Navigation;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using XamChat.Core.Fakes;
using XamChat.Core.Models;
using XamChat.Core.Services;
using XamChat.Core.ViewModels;

namespace XamChat.Tests
{
    [TestFixture]
    public class LoginViewModelTests
    {
        private LoginViewModel _loginViewModel;
        private IWebService _webService;
        private ISettings _settings;
        private Mock<IMvxNavigationService> _navigationService;

        [SetUp]
        public void SetUp()
        {
            _webService = new FakeWebService { SleepDuration = 0 };
            _settings = new FakeSettings();
            _navigationService = new Mock<IMvxNavigationService>();

            _loginViewModel = new LoginViewModel(_webService, _settings, _navigationService.Object);
        }

        [Test]
        public async Task LoginSuccessfully()
        {
            _loginViewModel.Username = "testuser";
            _loginViewModel.Password = "password";

            await _loginViewModel.Login();

            Assert.That(_settings.User, Is.Not.Null);
        }

        [Test]
        public async Task SaveSettingsMethodWasCalledWhenLoginSuccessfully()
        {
            _loginViewModel.Username = "testuser";
            _loginViewModel.Password = "password";

            await _loginViewModel.Login();

            Assert.That(((FakeSettings)_settings).SaveMethodWasCalled, Is.EqualTo(true));
        }

        [Test]
        public void LoginWithNoUsernameThrowsAnExceptionWithMessage()
        {
            _loginViewModel.Password = "password";

            Exception exception = Assert.ThrowsAsync<Exception>(async () => await _loginViewModel.Login());

            Assert.That(exception.Message, Is.EqualTo("Username is blank."));
        }

        [Test]
        public void LoginWithNoPasswordThrowsAnExceptionWithMessage()
        {
            _loginViewModel.Username = "testuser";

            Exception ex = Assert.ThrowsAsync<Exception>(async () => await _loginViewModel.Login());

            Assert.That(ex.Message, Is.EqualTo("Password is blank."));
        }

        [Test]
        public async Task NavigateMethodCalledWhenLoginIsSuccessfull()
        {
            _loginViewModel.Username = "testuser";
            _loginViewModel.Password = "password";

            await _loginViewModel.Login();

            _navigationService.Verify(x => x.Navigate<ConversationViewModel>(null));
        }


        [Test]
        public void NavigateMethodCalledWhenNavigationCommandExecuted()
        {
            _loginViewModel.OpenConversationViewCommand.Execute();

            _navigationService.Verify(x => x.Navigate<ConversationViewModel>(null));
        }
    }
}
