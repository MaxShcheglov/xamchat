﻿using Moq;
using MvvmCross.Core.Navigation;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;
using XamChat.Core.Fakes;
using XamChat.Core.Models;
using XamChat.Core.Services;
using XamChat.Core.ViewModels;

namespace XamChat.Tests
{
    [TestFixture]
    public class MessageViewModelTests
    {
        private IWebService _webService;
        private ISettings _settings;
        private MessageViewModel _messageViewModel;
        private Mock<IMvxNavigationService> _navigationService;

        [SetUp]
        public void SetUp()
        {
            _webService = new FakeWebService { SleepDuration = 0 };
            _settings = new FakeSettings();
            _navigationService = new Mock<IMvxNavigationService>();

            _messageViewModel = new MessageViewModel(_webService, _settings, _navigationService.Object);
        }

        [Test]
        public void GetMessagesWhenConversationNotExistsThrowsAnExceptionWithMessage()
        {
            var exception = Assert.ThrowsAsync<Exception>(async () => await _messageViewModel.GetMessages());

            Assert.That(exception.Message, Is.EqualTo("No conversation"));
        }

        [Test]
        public async Task SuccessfullyGetMessagesWhenConversationNotIsNull()
        {
            _messageViewModel.Conversation = new Conversation();

            await _messageViewModel.GetMessages();

            Assert.That(_messageViewModel.Messages, Is.Not.Null);
        }

        [Test]
        public void SendMessageWhenNotLoggedInThrowsAnExceptionWithMessage()
        {
            var exception = Assert.ThrowsAsync<Exception>(async () => await _messageViewModel.SendMessage());

            Assert.That(exception.Message, Is.EqualTo("Not logged in"));
        }

        [Test]
        public void SendMessageWhenConversationIsNullThrowsAnExceptionWithMessage()
        {
            _settings.User = new User();

            var exception = Assert.ThrowsAsync<Exception>(async () => await _messageViewModel.SendMessage());

            Assert.That(exception.Message, Is.EqualTo("No conversation"));
        }

        [Test]
        public void SendMessageWhenTextIsEmptyThrowsAnExceptionWithMessage()
        {
            _settings.User = new User();
            _messageViewModel.Conversation = new Conversation();

            var exception = Assert.ThrowsAsync<Exception>(async () => await _messageViewModel.SendMessage());

            Assert.That(exception.Message, Is.EqualTo("Message is blank"));
        }

        [Test]
        public async Task MessagesCollectionNotEmptyWhenSuccessfullySendMessage()
        {
            _settings.User = new User();
            _messageViewModel.Conversation = new Conversation();
            _messageViewModel.Text = "some text";

            await _messageViewModel.SendMessage();

            Assert.That(_messageViewModel.Messages, Is.Not.Null);
        }

        [Test]
        public async Task MessagesCollectionContainsTwoMessagesWhenSendMessageTwice()
        {
            _settings.User = new User();
            _messageViewModel.Conversation = new Conversation();
            _messageViewModel.Text = "some text";

            await _messageViewModel.SendMessage();
            await _messageViewModel.SendMessage();

            Assert.That(_messageViewModel.Messages.Count(), Is.EqualTo(2));
        }
    }
}
