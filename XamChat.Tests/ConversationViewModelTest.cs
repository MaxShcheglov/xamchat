﻿using Moq;
using MvvmCross.Core.Navigation;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using XamChat.Core.Fakes;
using XamChat.Core.Models;
using XamChat.Core.Services;
using XamChat.Core.ViewModels;

namespace XamChat.Tests
{
    [TestFixture]
    public class ConversationViewModelTests
    {
        private IWebService _webService;
        private ISettings _settings;
        private ConversationViewModel _conversationViewModel;
        private Mock<IMvxNavigationService> _navigationService;

        [SetUp]
        public void SetUp()
        {
            _webService = new FakeWebService { SleepDuration = 0 };
            _settings = new FakeSettings();
            _navigationService = new Mock<IMvxNavigationService>();

            _conversationViewModel = new ConversationViewModel(_webService, _settings, _navigationService.Object);
        }

        [Test]
        public void GetConversationsWhenNotLoggedInThrowsAnExceptionWithMessage()
        {
            var exception = Assert.ThrowsAsync<Exception>(async () => await _conversationViewModel.GetConversations());

            Assert.That(exception.Message, Is.EqualTo("Not logged in"));
        }

        [Test]
        public async Task SuccessfullyGetConversationsWhenLoggenIn()
        {
            _settings.User = new User();

            await _conversationViewModel.GetConversations();

            Assert.That(_conversationViewModel.Conversations, Is.Not.Null);
        }


        [Test]
        public void NavigateMethodCalledWhenNavigationCommandExecuted()
        {
            _conversationViewModel.OpenFriendsViewCommand.Execute();

            _navigationService.Verify(x => x.Navigate<FriendViewModel>(null));
        }   
    }
}
