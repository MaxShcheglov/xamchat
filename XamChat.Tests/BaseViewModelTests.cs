﻿using NUnit.Framework;
using XamChat.Core.ViewModels;

namespace XamChat.Tests
{
    [Ignore("now not needed")]
    [TestFixture]
    public class BaseViewModelTests
    {
        private BaseViewModel _baseViewModel;

        [SetUp]
        public void SetUp()
        {
            _baseViewModel = new BaseViewModel();
        }
    }
}
