﻿using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;
using XamChat.Core.Fakes;
using XamChat.Core.Models;
using XamChat.Core.Services;
using XamChat.Core.ViewModels;

namespace XamChat.Tests
{
    [TestFixture]
    public class FriendViewModelTests
    {
        private FriendViewModel _friendViewModel;
        private ISettings _settings;
        private IWebService _webService;

        private User[] _fiendsList = new User[] { new User(), new User(), new User() };

        [SetUp]
        public void SetUp()
        {
            _settings = new FakeSettings();
            _webService = new FakeWebService { SleepDuration = 0 };
            _friendViewModel = new FriendViewModel(_webService, _settings);
        }

        [Test]
        public async Task SuccessfullyGettingFriendsWhenLoggedIn()
        {
            _settings.User = new User();

            await _friendViewModel.GetFriends();

            Assert.That(_friendViewModel.Friends, Is.Not.Null);
        }

        [Test]
        public void FriendsRetrievingWhenNotLoggedInThrowsAnExceptionWithMessage()
        {
            var exception = Assert.ThrowsAsync<Exception>(async () => await _friendViewModel.GetFriends());

            Assert.That(exception.Message, Is.EqualTo("Not logged in"));
        }

        [Test]
        public void FriendsAddingWhenNotLoggedInThrowsAnExceptionWithMessage()
        {
            _friendViewModel.Username = "username";

            var exception = Assert.ThrowsAsync<Exception>(async () => await _friendViewModel.AddFriend());

            Assert.That(exception.Message, Is.EqualTo("Not logged in"));
        }

        [Test]
        public void FriendsAddingWithEmptyUsernameInThrowsAnExceptionWithMessage()
        {
            _settings.User = new User();

            var exception = Assert.ThrowsAsync<Exception>(async () => await _friendViewModel.AddFriend());

            Assert.That(exception.Message, Is.EqualTo("Username is blank"));
        }

        [Test]
        public async Task FriendsNotNullWhenFriendSuccessfullyAdded()
        {
            _settings.User = new User();
            _friendViewModel.Username = "username";

            await _friendViewModel.AddFriend();

            Assert.That(_friendViewModel.Friends, Is.Not.Null);
        }

        [Test]
        public async Task ReturnOneFriendWhenFriendSuccessfullyAdded()
        {
            _settings.User = new User();
            _friendViewModel.Username = "user";

            await _friendViewModel.AddFriend();

            Assert.That(_friendViewModel.Friends.Count(), Is.EqualTo(1));
        }

        [Test]
        public async Task ReturnTwoFriendsWhenFriendSuccessfullyAddedToNonEmptyFiendsList()
        {
            _settings.User = new User();
            _friendViewModel.Username = "user1";
            await _friendViewModel.AddFriend();

            _friendViewModel.Username = "user2";
            await _friendViewModel.AddFriend();

            Assert.That(_friendViewModel.Friends.Count(), Is.EqualTo(2));
        }
    }
}
