﻿using NUnit.Framework;
using System;
using System.Threading.Tasks;
using XamChat.Core.Fakes;
using XamChat.Core.Models;
using XamChat.Core.Services;
using XamChat.Core.ViewModels;

namespace XamChat.Tests
{
    [TestFixture]
    public class RegisterViewModelTests
    {
        private RegisterViewModel _registerViewModel;
        private IWebService _webService;
        private ISettings _settings;

        [SetUp]
        public void SetUp()
        {
            _webService = new FakeWebService { SleepDuration = 0 };
            _settings = new FakeSettings();

            _registerViewModel = new RegisterViewModel(_webService, _settings);
        }

        [Test]
        public void RegisterWithoutUsernameThrowsAnExceptionWithMessage()
        {
            _registerViewModel.Password = "password";
            _registerViewModel.ConfirmPassword = "password";

            Exception exception = Assert.ThrowsAsync<Exception>(async () => await _registerViewModel.Register());

            Assert.That(exception.Message, Is.EqualTo("Username is blank"));
        }

        [Test]
        public void RegisterWithoutPasswordThrowsAnExceptionWithMessage()
        {
            _registerViewModel.Username = "username";

            Exception exception = Assert.ThrowsAsync<Exception>(async () => await _registerViewModel.Register());

            Assert.That(exception.Message, Is.EqualTo("Password is blank"));
        }

        [Test]
        public void RegisterWithWrongConfirmPasswordThrowsAnExceptionWithMessage()
        {
            _registerViewModel.Username = "username";
            _registerViewModel.Password = "password";
            _registerViewModel.ConfirmPassword = "confirm password";

            Exception exception = Assert.ThrowsAsync<Exception>(async () => await _registerViewModel.Register());

            Assert.That(exception.Message, Is.EqualTo("Passwords don't match"));
        }

        [Test]
        public async Task RegisterSuccessfully()
        {
            _registerViewModel.Username = "username";
            _registerViewModel.Password = "password";
            _registerViewModel.ConfirmPassword = "password";

            await _registerViewModel.Register();

            Assert.That(_settings.User, Is.Not.Null);
        }

        [Test]
        public async Task SaveSettingsMethodWasCalledWhenRegister()
        {
            _registerViewModel.Username = "testuser";
            _registerViewModel.Password = "password";
            _registerViewModel.ConfirmPassword = "password";
        

            await _registerViewModel.Register();

            Assert.That(((FakeSettings)_settings).SaveMethodWasCalled, Is.EqualTo(true));
        }
    }
}
