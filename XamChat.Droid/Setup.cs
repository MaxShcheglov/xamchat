﻿using Android.Content;
using MvvmCross.Core.ViewModels;
using MvvmCross.Core.Views;
using MvvmCross.Droid.Platform;
using MvvmCross.Platform;
using System;
using System.Collections.Generic;
using XamChat.Core;
using XamChat.Core.ViewModels;
using XamChat.Droid.Views;

namespace XamChat.Droid
{
    public class Setup : MvxAndroidSetup
    {
        public Setup(Context applicationContext) : base(applicationContext)
        {
        }

        protected override IMvxApplication CreateApp()
        {
            return new App();
        }

        protected override void InitializeViewLookup()
        {
            var viewModelViewLookup = new Dictionary<Type, Type>()
            {
                { typeof(LoginViewModel), typeof(LoginView) },
                { typeof(MessageViewModel), typeof(MessagesView) },
                { typeof(FriendViewModel), typeof(FriendsView) },
                { typeof(ConversationViewModel), typeof(ConversationsView) }
            };

            var container = Mvx.Resolve<IMvxViewsContainer>();
            container.AddAll(viewModelViewLookup);
        }
    }
}