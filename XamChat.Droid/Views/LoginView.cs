﻿using System;
using Android.App;
using Android.OS;
using Android.Widget;
using XamChat.Core.ViewModels;

namespace XamChat.Droid.Views
{
    [Activity(Label = "@string/ApplicationName", MainLauncher = true)]
    public class LoginView : BaseView<LoginViewModel>
    {
        private EditText _username, _password;
        private Button _login;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Login);
            _username = FindViewById<EditText>(Resource.Id.username);
            _password = FindViewById<EditText>(Resource.Id.password);
            _login = FindViewById<Button>(Resource.Id.login);
            _login.Click += OnLogin;
        }

        protected override void OnResume()
        {
            base.OnResume();
            _username.Text = _password.Text = string.Empty;
        }

        private async void OnLogin(object sender, EventArgs e)
        {
            ViewModel.Username = _username.Text;
            ViewModel.Password = _password.Text;

            try
            {
                await ViewModel.Login();
            }
            catch (Exception ex)
            {
                DisplayError(ex);
            }
        }
    }
}