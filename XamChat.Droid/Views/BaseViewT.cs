﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Droid.Views;
using XamChat.Core.ViewModels;

namespace XamChat.Droid.Views
{
    [Activity]
    public class BaseView<TViewModel, TParameter> : MvxActivity<TViewModel> where TViewModel : BaseViewModel<TParameter>
    {
        private ProgressDialog _progressDialog;
        private bool _isProgressDialogVisible;

        public bool IsProgressDialogVisible
        {
            get => _isProgressDialogVisible;
            set
            {
                _isProgressDialogVisible = value;
                SetProgressDialogVisibility(_isProgressDialogVisible);
            }
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            _progressDialog = new ProgressDialog(this);
            _progressDialog.SetCancelable(false);
            _progressDialog.SetTitle(Resource.String.Loading);

            InitializeBinding();
        }

        private void InitializeBinding()
        {
            var set = this.CreateBindingSet<BaseView<TViewModel, TParameter>, TViewModel>();
            set.Bind(this).For(v => v.IsProgressDialogVisible).To(vm => vm.IsBusy);
            set.Apply();
        }

        private void SetProgressDialogVisibility(bool visible)
        {
            if (visible)
                _progressDialog.Show();
            else
                _progressDialog.Hide();
        }

        protected void DisplayError(Exception ex)
        {
            var error = ex.Message;

            new AlertDialog.Builder(this).SetTitle(Resource.String.ErrorTitle)
                .SetMessage(error)
                .SetPositiveButton(Android.Resource.String.Ok, (IDialogInterfaceOnClickListener)null)
                .Show();
        }
    }
}