﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using XamChat.Core.Models;
using XamChat.Core.ViewModels;

namespace XamChat.Droid.Views
{
    [Activity(Label = "Friends")]
    public class FriendsView : BaseView<FriendViewModel>
    {
        private ListView _listView;
        private Adapter _adapter;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Friends);
            _listView = FindViewById<ListView>(Resource.Id.friendsList);
            _listView.Adapter = _adapter = new Adapter(ViewModel, this);
        }

        protected override async void OnResume()
        {
            base.OnResume();
            try
            {
                await ViewModel.GetFriends();
                _adapter.NotifyDataSetInvalidated();
            }
            catch (Exception ex)
            {
                DisplayError(ex);
            }
        }

        public class Adapter : BaseAdapter<User>
        {
            private readonly FriendViewModel _friendsViewModel;
            private readonly LayoutInflater _inflater;

            public Adapter(FriendViewModel friendViewModel, Context context)
            {
                _friendsViewModel = friendViewModel;
                _inflater = (LayoutInflater)context.GetSystemService(LayoutInflaterService);
            }

            public override User this[int position] => _friendsViewModel.Friends[position];

            public override int Count => _friendsViewModel.Friends == null ? 0 : _friendsViewModel.Friends.Length;

            public override long GetItemId(int position) => _friendsViewModel.Friends.Length;

            public override View GetView(int position, View convertView, ViewGroup parent)
            {
                if (convertView == null)
                    convertView = _inflater.Inflate(Resource.Layout.FriendsListItem, null);

                var friend = this[position];
                var friendName = convertView.FindViewById<TextView>(Resource.Id.friendName);
                friendName.Text = friend.Username;

                return convertView;
            }
        }
    }
}