﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using System;
using XamChat.Core.Models;
using XamChat.Core.ViewModels;

namespace XamChat.Droid.Views
{
    [Activity(Label = "Conversations")]
    public class ConversationsView : BaseView<ConversationViewModel>
    {
        private Adapter _adapter;
        private ListView _listView;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Conversations);
            _listView = FindViewById<ListView>(Resource.Id.conversationsList);
            _listView.Adapter = _adapter = new Adapter(ViewModel, this);

            _listView.ItemClick += (sender, e) =>
            {
                var selectedConversation = _adapter[e.Position];

                ViewModel.OpenMessagesViewCommand.Execute(selectedConversation);
            };
        }

        protected override async void OnResume()
        {
            base.OnResume();
            try
            {
                await ViewModel.GetConversations();
                _adapter.NotifyDataSetInvalidated();
            }
            catch (Exception ex)
            {
                DisplayError(ex);
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.ConversationsMenu, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Resource.Id.addFriendMenu)
            {
                ViewModel.OpenFriendsViewCommand.Execute();
            }

            return base.OnOptionsItemSelected(item);
        }

        public class Adapter : BaseAdapter<Conversation>
        {
            private readonly ConversationViewModel _conversationViewModel;
            private readonly LayoutInflater _inflater;

            public Adapter(ConversationViewModel conversationViewModel, Context context)
            {
                _inflater = (LayoutInflater)context.GetSystemService(LayoutInflaterService);
                _conversationViewModel = conversationViewModel;
            }

            public override Conversation this[int position] => _conversationViewModel.Conversations[position];

            public override int Count => _conversationViewModel.Conversations == null ? 0 : _conversationViewModel.Conversations.Length;

            public override long GetItemId(int position)
            {
                return position;
            }

            public override View GetView(int position, View convertView, ViewGroup parent)
            {
                if (convertView == null)
                    convertView = _inflater.Inflate(Resource.Layout.ConversationListItem, null);

                var conversation = this[position];
                var username = convertView.FindViewById<TextView>(Resource.Id.conversationUsername);
                var lastMessage = convertView.FindViewById<TextView>(Resource.Id.conversationLastMessage);

                username.Text = conversation.Username;
                lastMessage.Text = conversation.LastMessage;

                return convertView;
            }
        }
    }
}