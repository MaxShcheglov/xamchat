﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using System;
using XamChat.Core.Models;
using XamChat.Core.ViewModels;
using Message = XamChat.Core.Models.Message;

namespace XamChat.Droid.Views
{
    [Activity(Label = "Messages")]
    public class MessagesView : BaseView<MessageViewModel, Conversation>
    {
        private ListView _listView;
        private EditText _messageText;
        private Button _sendButton;
        private Adapter _adapter;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Title = ViewModel.Conversation.Username;
            SetContentView(Resource.Layout.Messages);

            _listView = FindViewById<ListView>(Resource.Id.messageList);
            _messageText = FindViewById<EditText>(Resource.Id.messageText);
            _sendButton = FindViewById<Button>(Resource.Id.sendButton);

            _listView.Adapter = _adapter = new Adapter(this, ViewModel);

            _sendButton.Click += async (sender, e) =>
            {
                ViewModel.Text = _messageText.Text;
                try
                {
                    await ViewModel.SendMessage();
                    _messageText.Text = string.Empty;
                    _adapter.NotifyDataSetInvalidated();
                    _listView.SetSelection(_adapter.Count);
                }
                catch (Exception ex)
                {
                    DisplayError(ex);
                }
            };
        }

        protected override async void OnResume()
        {
            base.OnResume();
            try
            {
                await ViewModel.GetMessages();
                _adapter.NotifyDataSetInvalidated();
                _listView.SetSelection(_adapter.Count);
            }
            catch (Exception ex)
            {
                DisplayError(ex);
            }
        }

        public class Adapter : BaseAdapter<Message>
        {
            private const int MyMessageType = 0, TheirMessageType = 1;
            private readonly MessageViewModel _messageViewModel;
            private readonly ISettings _settings;
            private readonly LayoutInflater _inflater;

            public Adapter(Context context, MessageViewModel messageViewModel)
            {
                _inflater = (LayoutInflater)context.GetSystemService(LayoutInflaterService);
                _messageViewModel = messageViewModel;
                _settings = messageViewModel.Settings;
            }

            public override Message this[int position] => _messageViewModel.Messages[position];

            public override int Count => _messageViewModel.Messages == null ? 0 : _messageViewModel.Messages.Length;

            public override int ViewTypeCount => 2;

            public override long GetItemId(int position) => position;

            public override int GetItemViewType(int position)
            {
                var message = this[position];
                return message.UserId == _settings.User.Id ? MyMessageType : TheirMessageType;
            }

            public override View GetView(int position, View convertView, ViewGroup parent)
            {
                var message = this[position];
                int type = GetItemViewType(position);

                if (convertView == null)
                {
                    if (type == MyMessageType)
                    {
                        convertView = _inflater.Inflate(Resource.Layout.MyMessageListItem, null);
                    }
                    else
                    {
                        convertView = _inflater.Inflate(Resource.Layout.TheirMessageListItem, null);
                    }
                }

                TextView messageText, dateText;

                if (type == MyMessageType)
                {
                    messageText = convertView.FindViewById<TextView>(Resource.Id.myMessageText);
                    dateText = convertView.FindViewById<TextView>(Resource.Id.myMessageDate);
                }
                else
                {
                    messageText = convertView.FindViewById<TextView>(Resource.Id.theirMessageText);
                    dateText = convertView.FindViewById<TextView>(Resource.Id.theirMessageDate);
                }

                messageText.Text = message.Text;
                dateText.Text = message.Date.ToString("MM/dd/yy HH:mm");

                return convertView;
            }
        }
    }
}