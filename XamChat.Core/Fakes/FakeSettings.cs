﻿using XamChat.Core.Models;

namespace XamChat.Core.Fakes
{
    public class FakeSettings : ISettings
    {
        public bool SaveMethodWasCalled = false;

        public User User { get; set; }

        public void Save() => SaveMethodWasCalled = true;
    }
}
