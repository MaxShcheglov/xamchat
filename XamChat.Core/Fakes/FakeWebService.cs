﻿using System;
using System.Threading.Tasks;
using XamChat.Core.Models;
using XamChat.Core.Services;

namespace XamChat.Core.Fakes
{
    public class FakeWebService : IWebService
    {
        public FakeWebService()
        {
            SleepDuration = 1000;
        }

        public int SleepDuration { get; set; }

        public Task Sleep()
        {
            return Task.Delay(SleepDuration);
        }

        public async Task<User> Login(string username, string password)
        {
            await Sleep();
            return new User { Id = "1", Username = username };
        }

        public async Task<User> Register(User user)
        {
            await Sleep();
            return user;
        }

        public async Task<User[]> GetFriends(string userId)
        {
            await Sleep();
            return new[]
            {
                new User { Id = "2", Username = "bobama" },
                new User { Id = "3", Username = "bobloblaw" },
                new User { Id = "4", Username = "gmichael" },
            };
        }

        public async Task<User> AddFriend(string userId, string username)
        {
            await Sleep();
            return new User { Id = "5", Username = username };
        }

        public async Task<Conversation[]> GetConversations(string userId)
        {
            await Sleep();
            return new[]
            {
                new Conversation { Id = "1", UserId = "2", Username = "bobama", LastMessage = "Hey!" },
                new Conversation { Id = "1", UserId = "3", Username = "bobloblaw", LastMessage = "Have you seen that new movie?" },
                new Conversation { Id = "1", UserId = "4", Username = "gmichael", LastMessage = "What?" },
            };
        }

        public async Task<Message[]> GetMessages(string conversationId)
        {
            await Sleep();
            return new[]
            {
                new Message
                {
                    Id = "1",
                    ConversationId = conversationId,
                    UserId = "2",
                    Text = "Hey",
                    Date = DateTime.Now
                },
                new Message
                {
                    Id = "2",
                    ConversationId = conversationId,
                    UserId = "1",
                    Text = "What's up?",
                    Date = DateTime.Now
                },
                new Message
                {
                    Id = "3",
                    ConversationId = conversationId,
                    UserId = "2",
                    Text = "Have you seen that new movie",
                    Date = DateTime.Now
                },
                new Message
                {
                    Id = "4",
                    ConversationId = conversationId,
                    UserId = "1",
                    Text = "Its great!",
                    Date = DateTime.Now
                },
            };
        }

        public async Task<Message> SendMessage(Message message)
        {
            await Sleep();
            return message;
        }
    }
}
