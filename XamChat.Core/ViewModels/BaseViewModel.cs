﻿using MvvmCross.Core.ViewModels;

namespace XamChat.Core.ViewModels
{
    public class BaseViewModel : MvxViewModel
    {
        private bool _isBusy = false;

        public bool IsBusy
        {
            get => _isBusy;
            set => SetProperty(ref _isBusy, value);
        }
    }
}
