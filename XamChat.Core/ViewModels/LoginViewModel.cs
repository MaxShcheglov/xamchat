﻿using System;
using System.Threading.Tasks;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using XamChat.Core.Models;
using XamChat.Core.Services;

namespace XamChat.Core.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private readonly IWebService _webService;
        private readonly ISettings _settings;
        private readonly IMvxNavigationService _navigationService;

        public LoginViewModel(IWebService webService, ISettings settings, IMvxNavigationService navigationService)
        {
            _webService = webService;
            _settings = settings;
            _navigationService = navigationService;
        }

        public IMvxAsyncCommand OpenConversationViewCommand => new MvxAsyncCommand(async () => await _navigationService.Navigate<ConversationViewModel>());

        public string Username { get; set; }

        public string Password { get; set; }

        public async Task Login()
        {
            if (string.IsNullOrEmpty(Username))
                throw new Exception("Username is blank.");

            if (string.IsNullOrEmpty(Password))
                throw new Exception("Password is blank.");

            IsBusy = true;
            try
            {
                _settings.User = await _webService.Login(Username, Password);
                _settings.Save();

                OpenConversationViewCommand.Execute();
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
