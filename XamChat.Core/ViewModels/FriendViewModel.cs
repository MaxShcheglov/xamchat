﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using XamChat.Core.Models;
using XamChat.Core.Services;

namespace XamChat.Core.ViewModels
{
    public class FriendViewModel : BaseViewModel
    {
        private readonly IWebService _webService;
        private readonly ISettings _settings;

        public FriendViewModel(IWebService webService, ISettings settings)
        {
            _webService = webService;
            _settings = settings;
        }

        public User[] Friends { get; private set; }
        public string Username { get; set; }

        public async Task GetFriends()
        {
            if (_settings.User == null)
                throw new Exception("Not logged in");

            IsBusy = true;
            try
            {
                Friends = await _webService.GetFriends(_settings.User.Id);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public async Task AddFriend()
        {
            if (_settings.User == null)
                throw new Exception("Not logged in");

            if (string.IsNullOrEmpty(Username))
                throw new Exception("Username is blank");

            IsBusy = true;
            try
            {
                var friend = await _webService.AddFriend(_settings.User.Id, Username);

                var friends = new List<User>();
                if (Friends != null)
                    friends.AddRange(Friends);
                friends.Add(friend);

                Friends = friends.OrderBy(f => f.Username).ToArray();
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
