﻿using System;
using System.Threading.Tasks;
using XamChat.Core.Models;
using XamChat.Core.Services;

namespace XamChat.Core.ViewModels
{
    public class RegisterViewModel : BaseViewModel
    {
        private readonly IWebService _webService;
        private readonly ISettings _settings;

        public RegisterViewModel(IWebService webService, ISettings settings)
        {
            _webService = webService;
            _settings = settings;
        }

        public string Username { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public async Task Register()
        {
            if (string.IsNullOrEmpty(Username))
                throw new Exception("Username is blank");
            if (string.IsNullOrEmpty(Password))
                throw new Exception("Password is blank");
            if (Password != ConfirmPassword)
                throw new Exception("Passwords don't match");

            IsBusy = true;
            try
            {
                _settings.User = await _webService.Register(new User { Username = Username, Password = Password });
                _settings.Save();
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
