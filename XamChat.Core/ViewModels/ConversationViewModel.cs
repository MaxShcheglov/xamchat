﻿using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using System;
using System.Threading.Tasks;
using XamChat.Core.Models;
using XamChat.Core.Services;

namespace XamChat.Core.ViewModels
{
    public class ConversationViewModel : BaseViewModel
    {
        private readonly IWebService _webService;
        private readonly ISettings _settings;
        private readonly IMvxNavigationService _navigationService;

        public ConversationViewModel(IWebService webService, ISettings settings, IMvxNavigationService navigationService)
        {
            _webService = webService;
            _settings = settings;
            _navigationService = navigationService;
        }

        public IMvxAsyncCommand<Conversation> OpenMessagesViewCommand => new MvxAsyncCommand<Conversation>(async (conversation) => await _navigationService.Navigate<MessageViewModel, Conversation>(conversation));

        public IMvxAsyncCommand OpenFriendsViewCommand => new MvxAsyncCommand(async () => await _navigationService.Navigate<FriendViewModel>());

        public Conversation[] Conversations { get; private set; }

        public async Task GetConversations()
        {
            if (_settings.User == null)
                throw new Exception("Not logged in");

            IsBusy = true;
            try
            {
                Conversations = await _webService.GetConversations(_settings.User.Id);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
