﻿using MvvmCross.Core.ViewModels;

namespace XamChat.Core.ViewModels
{
    public class BaseViewModel<T> : MvxViewModel<T>
    {
        private bool _isBusy = false;

        public bool IsBusy
        {
            get => _isBusy;
            set => SetProperty(ref _isBusy, value);
        }

        protected T Parameter { get; private set; }

        public override void Prepare(T parameter)
        {
            Parameter = parameter;
        }
    }
}
