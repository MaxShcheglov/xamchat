﻿using MvvmCross.Core.Navigation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using XamChat.Core.Models;
using XamChat.Core.Services;

namespace XamChat.Core.ViewModels
{
    public class MessageViewModel : BaseViewModel<Conversation>
    {
        private readonly IWebService _webService;
        private readonly ISettings _settings;
        private readonly IMvxNavigationService _navigationService;

        public MessageViewModel(IWebService webService, ISettings settings, IMvxNavigationService navigationService)
        {
            _webService = webService;
            _settings = settings;
            _navigationService = navigationService;
        }

        public Conversation Conversation { get; set; }
        public Message[] Messages { get; private set; }
        public string Text { get; set; }
        public ISettings Settings => _settings;

        public override void Prepare(Conversation conversation)
        {
            base.Prepare(conversation);

            Conversation = conversation;
        }

        public async Task GetMessages()
        {
            if (Conversation == null)
                throw new Exception("No conversation");

            IsBusy = true;
            try
            {
                Messages = await _webService.GetMessages(Conversation.Id);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public async Task SendMessage()
        {
            if (_settings.User == null)
                throw new Exception("Not logged in");

            if (Conversation == null)
                throw new Exception("No conversation");

            if (string.IsNullOrEmpty(Text))
                throw new Exception("Message is blank");

            IsBusy = true;
            try
            {
                var message = await _webService.SendMessage(new Message
                {
                    UserId = _settings.User.Id,
                    ConversationId = Conversation.Id,
                    Text = Text
                });

                var messages = new List<Message>();
                if (Messages != null)
                    messages.AddRange(Messages);
                messages.Add(message);

                Messages = messages.ToArray();
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}