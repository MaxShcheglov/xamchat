﻿namespace XamChat.Core.Models
{
    public class User
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public bool EqualTo(User user)
        {
            return Id == user.Id
                && Username == user.Username
                && Password == user.Password;
        }
    }
}
