﻿namespace XamChat.Core.Models
{
    public interface ISettings
    {
        User User { get; set; }
        void Save();
    }
}
