using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using XamChat.Core.Fakes;
using XamChat.Core.Models;
using XamChat.Core.Services;
using XamChat.Core.ViewModels;

namespace XamChat.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            Mvx.RegisterSingleton<ISettings>(new FakeSettings());
            Mvx.RegisterSingleton<IWebService>(new FakeWebService());

            RegisterNavigationServiceAppStart<LoginViewModel>();
        }
    }
}
